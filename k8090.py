import struct
from serial import Serial

STX = 0x04
ETX = 0x0f

K8090_SWITCH_RELAY_ON = 0x11
K8090_SWITCH_RELAY_OFF = 0x12
K8090_TOGGLE_RELAY = 0x14

class K8090Packet(object):
    def __init__(self, command, mask, param1, param2):
        self.command = command
        self.mask = mask
        self.param1 = param1
        self.param2 = param2

    def checksum(self):
        n = sum([STX, self.command, self.mask, self.param1, self.param2]) % 256
        return (~n & 0xFF) + 1

    def pack(self):
        return struct.pack('BBBBBBB', STX, self.command, self.mask, self.param1, self.param2, self.checksum(), ETX)

class K8090Controller(object):
    def __init__(self, dev):
        self.dev = dev

    def toggle(self, mask):
        packet = K8090Packet(K8090_TOGGLE_RELAY, mask, 0, 0)
        self.send(packet.pack())

    def switch_on(self, mask):
        packet = K8090Packet(K8090_SWITCH_RELAY_ON, mask, 0, 0)
        self.send(packet.pack())

    def switch_off(self, mask):
        packet = K8090Packet(K8090_SWITCH_RELAY_OFF, mask, 0, 0)
        self.send(packet.pack())

    def send(self, data):
        with Serial(self.dev, 19200, timeout=1) as s:
            s.write(data)
