#!/bin/bash

root="$(dirname "$0")"

source "$root"/.venv/bin/activate

"$root"/main.py "$@"
