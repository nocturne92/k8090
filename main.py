#!/usr/bin/env python3

import sys
from argparse import ArgumentParser
from k8090 import K8090Controller

def main():
    parser = ArgumentParser()

    parser.add_argument('device', metavar='DEV', nargs=1)
    parser.add_argument('--on', dest='mode', action='store_const', const='on')
    parser.add_argument('--off', dest='mode', action='store_const', const='off')
    parser.add_argument('--toggle', dest='mode', action='store_const', const='toggle')
    parser.add_argument('--mask')

    arg = parser.parse_args()

    if arg.mode == None:
        parser.print_usage()
        print('mode argument missing')

        sys.exit(1)

    if arg.mask == None:
        parser.print_usage()
        print('mask argument missing')

        sys.exit(1)

    dev = arg.device[0]
    mask = int(arg.mask)

    controller = K8090Controller(dev)
    
    if arg.mode == 'on':
        controller.switch_on(mask)
    elif arg.mode == 'off':
        controller.switch_off(mask)
    elif arg.mode == 'toggle':
        controller.toggle(mask)

if __name__ == '__main__':
    main()
