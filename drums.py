import time
import math
import curses
from threading import Thread
from mido import MidiFile
from inspect import getmembers
from argparse import ArgumentParser

from k8090 import K8090Controller

class Drums(object):
    def __init__(self, controller):
        self.controller = controller

    def _tick(self, mask):
        self.controller.toggle(mask)

        #self.controller.switch_off(mask)
        #time.sleep(.001)
        #self.controller.switch_on(mask)

    def _emit(self, delay):
        self._tick(1)
        time.sleep(delay)
        self._tick(1 << 1)
        time.sleep(delay)
        self._tick(1 << 2)
        time.sleep(delay)
        self._tick(1 << 3)
        time.sleep(delay)
        self._tick(1 << 4)
        time.sleep(delay)
        self._tick(1 << 5)
        time.sleep(delay)
        self._tick(1 << 6)
        time.sleep(delay)
        self._tick(1 << 7)
        time.sleep(delay)

        return delay * 8

    def a(self):
        return self._emit(.005)

    def b(self):
        return self._emit(.0001)

    def c(self):
        self.controller.toggle(255)
        return 0

    def d(self):
        return self._emit(.01)

def sinedrums(device):
    drums = Drums(K8090Controller(device))

    a = 1/4
    b = 1/8
    c = b - a

    t = 0

    while True:
        delay = math.sin(t) * c + c/2 + a

        drums.a()
        time.sleep(delay)
        drums.b()
        time.sleep(delay)
        drums.c()
        time.sleep(delay)

        t += delay * 4

def keyboard(device):
    drums = Drums(K8090Controller(device))

    try:
        scr = curses.initscr()
        curses.noecho()
        curses.cbreak()

        while True:
            key = scr.getkey()

            if key == 'q':
                drums.a()
            elif key == 'w':
                drums.b()
            elif key == 'e':
                drums.c()
            elif key == 'r':
                drums.d()
    finally:
        curses.endwin()

def play_midi(device, path):
    drums = Drums(K8090Controller(device))
    midi = MidiFile(path)

    for msg in midi:
        time.sleep(msg.time)

        a_notes = [36]
        b_notes = [42]
        c_notes = [37]
        d_notes = [51]

        if msg.type == 'note_on' and msg.channel == 9:
            if msg.note in a_notes:
                Thread(target=drums.a, args=()).start()
            elif msg.note in b_notes:
                Thread(target=drums.b, args=()).start()
            elif msg.note in c_notes:
                Thread(target=drums.c, args=()).start()
            elif msg.note in d_notes:
                Thread(target=drums.d, args=()).start()
            print(msg.note)

if __name__ == '__main__':
    parser = ArgumentParser()

    parser.add_argument('device', metavar='DEV', nargs=1)
    parser.add_argument('--sine', dest='mode', action='store_const', const='sine')
    parser.add_argument('--keyboard', dest='mode', action='store_const', const='keyboard')
    parser.add_argument('--midi')

    arg = parser.parse_args()

    if arg.mode == None and arg.midi == None:
        parser.print_usage()
        print('Mode argument missing')
    elif arg.mode == 'sine':
        sinedrums(arg.device[0])
    elif arg.mode == 'keyboard':
        keyboard(arg.device[0])
    elif arg.midi != None:
        play_midi(arg.device[0], arg.midi)
